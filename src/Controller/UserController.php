<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Response;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


/**
 * @Route("/api/user", name="api_user")
 */


class UserController extends Controller
{
    private $serializer;

    public function __construct()
    {
        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceLimit(1);
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });
        $this->serializer = new Serializer([$normalizer], [$encoder]);
    }

    /**
     * @Route("/{id}", name="one_user", methods="GET")
     */
    public function findById(User $user) : Response
    {
        $data = $this->serializer->normalize($user, null, ['attributes' => ['id', 'email', 'password', 'contacts']]);
        $response = new Response($this->serializer->serialize($user, "json"));
        return $response;
    }

    /**
     * @Route("", name="all_users", methods={"GET"})
     */
    public function all(UserRepository $repo)
    {
        $list = $repo->findAll();

        $data = $this->serializer->normalize($list, null, ['attributes' => ['id', 'email', 'password', 'contacts']]);

        $response = new Response($this->serializer->serialize($data, 'json'));
        return $response;
    }

    /**
     * @Route("", name="new_user", methods={"POST"})
     */
    public function addUser(Request $request, UserPasswordEncoderInterface $encoder) : Response
    {
        $manager = $this->getDoctrine()->getManager();

        $content = $request->getContent();
        $user = $this->serializer->deserialize($content, User::class, "json");
        $user->setPassword($encoder->encodePassword($user, $user->getPassword()));

        $manager->persist($user);
        $manager->flush();

        $data = $this->serializer->normalize($user, null, ['attributes' => ['id', 'email', 'password', 'contacts']]);

        $response = new Response($this->serializer->serialize($user, "json"));
        return $response;
    }

    /**
     * @Route("/{id}", name="update_user", methods={"PUT"})
     */
    public function update(Request $request, User $user, UserPasswordEncoderInterface $encoder)
    {
        $manager = $this->getDoctrine()->getManager();

        $content = $request->getContent();
        $update = $this->serializer->deserialize($content, User::class, "json");

        $user->setEmail($update->getEmail());
        $user->setPassword($encoder->encodePassword($user, $update->getPassword()));

        $manager->persist($user);
        $manager->flush();

        $data = $this->serializer->normalize($user, null, ['attributes' => ['id', 'email', 'password', 'contacts']]);

        $response = new Response($this->serializer->serialize($data, "json"));
        return $response;
    }

    /**
     * @Route("/{id}", name="delete_user", methods={"DELETE"})
     */
    public function delete(User $user)
    {
        $manager = $this->getDoctrine()->getManager();

        $manager->remove($user);
        $manager->flush();

        return new Response("OK", 204);
    }
}
